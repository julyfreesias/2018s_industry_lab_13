package ictgradschool.industry.concurrency.ex05;


import ictgradschool.Keyboard;
import ictgradschool.industry.concurrency.examples.example03.PrimeFactors;

import java.util.List;



/*1. The usual main thread that starts the other two threads and which waits for the computation thread to complete.
2. The computation thread that is used to run the ​PrimeFactorsTask​.
3. The abort thread that is used to block on user input. When the user types a
key, indicating that they want to abort the computation, this thread should
interrupt the computation thread via an ​interrupt()​ call.*/

public class Main {
    static boolean cancelled = false;
    public static void main(String[] args) {

        System.out.println("Enter your prime number");

        Long userInput = Long.parseLong(Keyboard.readInput());

        PrimeFactorsTask primeFactorsTask = new PrimeFactorsTask(userInput);

        Thread thread = new Thread(primeFactorsTask);

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                String userInput = Keyboard.readInput();
                thread.interrupt();
                System.out.println("Thread interrupted");
                cancelled= true;

            }
        });
        thread.start();
        thread1.start();
        try{
            thread.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        List<Long> l = primeFactorsTask.getPrimeFactors();
        if(!cancelled){
        for(Long prime : l){
            System.out.println(prime + "\n");
        }
        }
    }
}
/*​PrimeFactorsTask​ needs to participate in the abortion process.
Whenever the abort thread is interrupted, the ​PrimeFactorsTask
needs to become aware that it should cease processing.
A common pattern in such situations is to have a ​Runnable​ object’s ​run()​ method
periodically check its Thread​’s interrupted status,
and for the ​run()​ method to finish upon detecting the interrupt.
For ​PrimeFactorsTask​, you can add such a check inside the for loop of the prime factors algorithm.
​Thread​’s class (as opposed to instance) method*/