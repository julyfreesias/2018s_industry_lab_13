package ictgradschool.industry.concurrency.ex05;


/*In addition, ​PrimeFactorsTask​ should define a public enumerated type
named ​TaskState with three values:
​Initialized​, ​Completed​ and ​Aborted​.
The ​Aborted​ value won’t be required until step 3, but it can be added now.*/

public enum TaskState {
    INITIALIZED, COMPLEATED, ABORTED
}
