package ictgradschool.industry.concurrency.ex05;

import ictgradschool.Keyboard;
import ictgradschool.industry.concurrency.examples.example03.PrimeFactors;
import java.util.ArrayList;
import java.util.List;


/*This class will implement the prime factorization algorithm
and should provide the following public methods.
● PrimeFactorsTask(longn)
● voidrun()
● longn()
● List<Long>getPrimeFactors()throwsIllegalStateException
● TaskStategetState()*/

public class PrimeFactorsTask implements Runnable {

    private long n;
    /*also involves instantiating a ​List<Long>​ instance variable,
        which will later be used to store computed prime numbers,
        and an instance variable of type ​TaskState​,
        assigned the value Initialized.*/
    private List<Long> primeFactors;
    private TaskState taskState;

    public PrimeFactorsTask (long n){
        this.n = n;

        /*The constructor should create a ​PrimeFactorsTask​ object
        with a given value for N (the value for which prime factors
        are to be computed).*/
//        long userInput= Long.parseLong(Keyboard.readInput());
//        PrimeFactorsTask pFT = new PrimeFactorsTask(userInput);

        /*Initializing the ​PrimeFactorsTask​ object
        */
        taskState = TaskState.INITIALIZED;

    }

    /*Method ​run()​ should implement the algorithm for computing prime factors,
     storing the computed primes in the ​PrimeFactorTask​’s ​List​ instance variable.
     When the algorithm has completed,
     ​run()​ should set the ​PrimeFactorsTask​ object’s ​TaskState​ instance variable to Completed.
     */
    @Override
    public void run() {
        primeFactors = new ArrayList<>();

        for (long i = 2; i*i <= n; i++) {
            //inside for loop check the current Thread is interrupted.
            if(Thread.currentThread().isInterrupted()){
                System.out.println("computation has been aborted");
                break;
            }
            // if factor is a factor of n, repeatedly divide it out
            while (n % i == 0) {
                System.out.print(i + " ");
                n = n / i;
            }
        }

        // if biggest factor occurs only once, n > 1
        if (n > 1) {
            primeFactors.add(n);
            System.out.println(n);
        } else  {  System.out.println();
        }
        taskState=TaskState.COMPLEATED;
    }

    //n()​ simply returns the value of N for which the ​PrimeFactorsTask​ object has been created.
    public double n(){
        return n;
    }


    /*The ​getPrimeFactors()​ method is intended to be called
     only after the ​run()​ method has completed
    In cases where the algorithm has completed,
    this method should return the List of computed prime factors.
    In other cases, it should throw the standard Java run-time exception ​
    IllegalStateException​.*/



    public List<Long> getPrimeFactors() throws IllegalStateException{
        return primeFactors;
    }

    /*Finally, method ​getState()​ returns the current state of the ​PrimeFactorsTask​ instance.
    Having implemented ​PrimeFactorsTask​, you have a class whose objects are intended to be executed
    by ​Thread​ instances. When a ​Thread​ is created and passed a PrimeFactorsTask​ object,
    starting the Thread object will cause the new thread to execute the ​PrimeFactorsTask​’s ​run()​ method.
    The ​PrimeFactorsTask​ object, like any object, has state that it modifies over time.
    Once the thread has finished executing, the associated PrimeFactorsTask​ object
    can be inspected for the computed prime factors.*/

    public TaskState getState(TaskState TaskState){
        return TaskState;
    }




   /*The constructor should create a ​PrimeFactorsTask​ object with a given value
      for N (the value for which prime factors are to be computed).*/

}






