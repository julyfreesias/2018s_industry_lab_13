package ictgradschool.industry.concurrency.ex04;


import java.util.concurrent.BlockingQueue;


/*For the two consumers, a new class ​Consumer should be introduced.
* Consumer should implement Runnable so that a ​Consumer instance
* can be passed to a ​Thread object for execution. */

public class Consumer implements Runnable {

    //producer/consumer 양쪽에서 공유할 큐 blockingQueue 로 제어
    /*Consumer​’s instance variables should include references
     * to the shared BlockingQueue​ and ​BankAccount​ objects.*/
    BlockingQueue<Transaction> bQueue;
    BankAccount  myBankAccount= new BankAccount();

    public Consumer(BankAccount bA, BlockingQueue<Transaction> queue) {
        this.myBankAccount = bA;
        this.bQueue = queue;

    }

    @Override
    public void run() {
        try{
            while (true){
                //트랜잭션 instanciate 하면서 정의-blockingQueue 리스트에서 가져온다(take())
                Transaction currentTransaction = bQueue.take();
                //가져온 리스트 하나(정의한 베리어블) 의 타입(트랜젝션클래스에 정의된) 이 트랜잭션의 타입의 withdraw 와 같을때,
                if (currentTransaction._type == Transaction.TransactionType.Withdraw ){
                    //은행걔좌에서 출금() 하겠다.
                    myBankAccount.withdraw(currentTransaction._amountInCents);
                }else if (currentTransaction._type == Transaction.TransactionType.Deposit){
                    myBankAccount.deposit(currentTransaction._amountInCents);
                } else if (currentTransaction == null) {
                    break;
                }
            }

        } catch (InterruptedException i){}
        /*
        put(),take() 메서드 콜하는게 소비자쓰레드가 interrupted 되었다는 소리.
        For ​Consumer​, catching an InterruptedException from a ​put() ​or ​take() call
        on the ​BlockingQueue indicates that the consumer thread has been interrupted
        (e.g., by ConcurrentBankingApp having detected that the producer
        has finished putting Transactions into the queue)*/



    }
}
