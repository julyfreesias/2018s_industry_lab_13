package ictgradschool.industry.concurrency.ex04;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class ConcurrentBankingApp {
    public static void main(String[] args) {



        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        BankAccount bankAccnt = new BankAccount();


        //1. Create the three threads.
        //The most convenient way of implementing the single producer is
        // through an anonymous inner class of type Runnable.
        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Transaction> transactions =TransactionGenerator.readDataFile();
                for(Transaction transaction : transactions){
                    try{
                        queue.put(transaction);
                    }catch (InterruptedException i){}
                }

            }
        });

        //For the two consumers, a new class ​Consumer should be introduced.
        // This class can be instantiated twice, once for each consumer.
        Thread consumer1 = new Thread(new Consumer(bankAccnt,queue));
        Thread consumer2 = new Thread(new Consumer(bankAccnt,queue));

        //2. Start each thread.
        producer.start();
        consumer1.start();
        consumer2.start();


        try {
            //3. Wait for the producer to finish putting ​Transaction items into the queue and
            //terminate.
            producer.join();


            while(queue.poll()!=null){

                /*4. Once the producer has finished, send an interrupt request to the two consumers.
                The consumers should respond by terminating as soon as the queue becomes empty
                after receiving the interrupt request.
                Note that if the consumers terminate because the queue is empty before receiving the interrupt,
                there may be more data to arrive in the queue that wouldn’t be processed.*/

                consumer1.interrupted();
                consumer2.interrupted();

                //5. Wait for the two consumers to terminate.
                consumer1.join();
                consumer2.join();
            }
        }

        /*You should handle ​InterruptedException appropriately.
        Note that this exception can be thrown by methods that block the calling thread,
        e.g. ​BlockingQueue’​ s ​put() and ​take() methods and Thread’s ​join() method.
       .*/
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        //6. Print out the final balance of the shared ​BankAccount​ object.
        System.out.println("Final balance is:" + bankAccnt.getFormattedBalance());

    }

}
