package ictgradschool.industry.concurrency.ex03;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        // TODO Implement this.

        final int threadNo = 32;
        Double[] result = new Double[threadNo];
        Thread[] threads = new Thread[threadNo];

        for(int i=0; i<threadNo; i++){

            int finalI = i;
            Thread myThread = new Thread(new Runnable() {
                @Override
                public void run() {

                    result[finalI]=ExerciseThreeMultiThreaded.super.estimatePI(numSamples/threadNo); //앤드류가 말하길 이상한 코드지만 이런방법으로 상속받는 클래스 메서드 가져올수 있음.
                }
            });
            myThread.start();
            threads[i]=myThread;
        }
        try {

            for (int i = 0; i < threadNo; i++) {
                threads[i].join(); //앞에 for loop 에 모든 룹을 기다리는 코드. 하나하고 기다리는건 의미가 없음.
            }

        }catch(InterruptedException e){}

        Double sumOfResult =0.0;

        for(int i =0; i<threadNo ; i++){

            sumOfResult = sumOfResult+result[i] ; //그냥 i 말고 result 의 i

        }
        return sumOfResult/threadNo;
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
